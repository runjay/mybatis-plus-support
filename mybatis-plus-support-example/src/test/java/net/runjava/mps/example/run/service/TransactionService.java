package net.runjava.mps.example.run.service;

import net.runjava.mps.example.mapper.TestStudentMapper;
import net.runjava.mps.example.model.TestStudent;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Running
 * @since 2024/3/14 19:32
 */

@Service
public class TransactionService {

    @Resource
    private TestStudentMapper testStudentMapper;


    public void testIncr(){

        TestStudent insertModel = new TestStudent();
        insertModel.setAge(10);
        insertModel.setName("王万");
        insertModel.setClassId(1L);
        testStudentMapper.insert(insertModel);

        Long id = 1L;
        int incrAgeValue = 1;
        TestStudent testStudent = testStudentMapper.selectById(id);

        Integer oriAge = testStudent.getAge();
        if(oriAge == null){
            throw new RuntimeException("Age is null, not support incr ");
        }
        TestStudent updateModel = new TestStudent();
        // age自增的值
        updateModel.setAge(incrAgeValue);
        updateModel.setId(id);
        // 第二参数需要自增的参数集合
        testStudentMapper.incrUpdateById(updateModel,TestStudent::getAge);

        TestStudent afterIncr = testStudentMapper.selectById(id);
//        Assert.isTrue(afterIncr.getAge().longValue() == oriAge + incrAgeValue ,"结果不正确");
    }

}
