package net.runjava.mps.example.run;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import net.runjava.mps.example.entity.QueryStudentForm;
import net.runjava.mps.example.mapper.TestStudentMapper;
import net.runjava.mps.example.model.TestStudent;
import net.runjava.mps.example.service.TestStudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import javax.annotation.Resource;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/5 19:23
 */


@RunWith(SpringRunner.class)
@SpringBootTest
public class QueryFormTest {


    @Resource
    private TestStudentService testStudentService;

    @Resource
    private TestStudentMapper testStudentMapper;



    /**
     * 单表条件过滤，过滤 QueryStudentForm其他表的字段
     */
    @Test
    public void testSingleTableQuery(){
        QueryStudentForm queryStudentForm = new QueryStudentForm();
        queryStudentForm.setPageNo(1L);
        queryStudentForm.setPageSize(10L);
        queryStudentForm.setClassCode("class1");
        queryStudentForm.setAge(new Integer[]{2,31});
        // 只查询主表数据，忽略关联表字段
        IPage page = testStudentService.selectPage(queryStudentForm);
        System.out.println("总数据条数" + page.getTotal());
    }


    /**
     * 结合mybatis 原生xml sql语句使用
     */
    @Test
    public void testMybatisNativeSqlQuery(){
        QueryStudentForm queryStudentForm = new QueryStudentForm();
        queryStudentForm.setName("李"); //姓李的
        queryStudentForm.setPageNo(1L);
        queryStudentForm.setPageSize(10L);
        queryStudentForm.setAge(new Integer[]{9,13});
        queryStudentForm.setClassId(1L);
        queryStudentForm.setClassCode("class1");
        queryStudentForm.setIds(new Long[]{1L,3L,5L});
        queryStudentForm.setClassName("1班");
        queryStudentForm.setSortBy("id");
        queryStudentForm.setSortType("desc");

        IPage page1 = testStudentMapper.queryStudent2Map(queryStudentForm.buildPage(),queryStudentForm);
        // 自定义条件清空
        queryStudentForm.setClassName(null);
        IPage page2 = testStudentMapper.queryStudent2Map(queryStudentForm.buildPage(),queryStudentForm);

    }
}
