package net.runjava.mps.example.run;


import net.runjava.mps.example.mapper.TestStudentMapper;
import net.runjava.mps.example.model.TestStudent;
import net.runjava.mps.example.run.service.TransactionService;
import org.apache.ibatis.session.SqlSession;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:35
 */


@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseMapperSupportTest {

    @Resource
    private TestStudentMapper testStudentMapper;

    @Resource
    private TransactionService transactionService;



    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    @Test
    public void test(){
        Long id = 1L;
        TestStudent testStudent = testStudentMapper.selectById(id);

        // 在更新当前记录时，要判断名字是否唯一
        // 由于要过滤本身，因此是false
        boolean ex2 = testStudentMapper.exists(testStudent,TestStudent::getName);
        Assert.assertEquals(ex2,false);


        TestStudent insert = new TestStudent();
        insert.setName(testStudent.getName());
        // 插入记录时，判断名字是否存在  与id为1的记录名字相同。 因此返回的是true
        boolean ex1 = testStudentMapper.exists(insert,TestStudent::getName);
        Assert.assertEquals(ex1,true);
    }


    @Test
    public void testIncr(){
        Long id = 1L;
        int incrAgeValue = 1;
        TestStudent testStudent = testStudentMapper.selectById(id);

        Integer oriAge = testStudent.getAge();
        if(oriAge == null){
            throw new RuntimeException("Age is null, not support incr ");
        }

        TestStudent updateModel = new TestStudent();
        // age自增的值
        updateModel.setAge(incrAgeValue);
        updateModel.setId(id);
        // 更新成丽丽
        updateModel.setName("丽丽");
        // 第二参数需要自增的参数集合
        testStudentMapper.incrUpdateById(updateModel,TestStudent::getAge);

        TestStudent afterIncr = testStudentMapper.selectById(id);
        Assert.assertEquals(afterIncr.getAge().longValue(),oriAge + incrAgeValue);
    }

}
