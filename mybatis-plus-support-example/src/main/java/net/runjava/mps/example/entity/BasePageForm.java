package net.runjava.mps.example.entity;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.Data;
import net.runjava.mps.query.form.AbstractQueryForm;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/9 20:54
 */

@Data
public abstract class BasePageForm<T> extends AbstractQueryForm<T> {

    private static final String SORT_TYPE_DESC = "DESC";

    private String sortBy;

    private String sortType;

    private Long pageSize;

    private Long pageNo;


    /**
     * 为了暴露字段名或表别名
     * 子类可以重写该函数，设置映射关系
     * 例如：
     *      sql = SELECT * FROM table t1
     *              LEFT JOIN table2 t2 ON(t1.c = t2.id)
     *             WHERE  t1.name = 'aa'
     *      sortColumnMap = {"id":"t1.id","tid":"t2.id"}
     *  当sortBy = "id" 实际排序的字段是 t1.id
     * @return 排序字段名与数据库字段名的映射关系
     */
    protected Map<String,String> getSortColumnMap(){
        return Collections.EMPTY_MAP;
    }


    @Override
    public long getPageSize() {
        if(pageSize == null){
            return 10;
        }
        return pageSize;
    }

    @Override
    public long getPageNo() {
        if(pageNo == null || pageNo <= 0){
            return 1;
        }
        return pageNo;
    }

    @Override
    public List<OrderItem> getOrders() {
        if(StringUtils.isBlank(sortBy)){
            return null;
        }
        List<OrderItem> orderItems = new ArrayList<>();
        String column = getSortColumnMap().get(sortBy);
        if(StringUtils.isBlank(column)){
            column = sortBy;
        }
        if(SORT_TYPE_DESC.equalsIgnoreCase(sortType)){
            orderItems.add(OrderItem.desc(column));
        }else{
            orderItems.add(OrderItem.asc(column));
        }
        return orderItems;
    }


    /**
     * 预留给子类重写，用于设置默认排序方式
     * @return
     */
    @Override
    public List<OrderItem> getDefaultOrders() {
        return null;
    }
}
