package net.runjava.mps.example.run;

import net.runjava.mps.example.mapper.TestStudentMapper;
import net.runjava.mps.example.model.TestStudent;
import net.runjava.mps.example.service.TestStudentService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Running
 * @since 2024/3/14 21:21
 */

@Component
public class Runner implements CommandLineRunner {


    @Resource
    TestStudentService testStudentService;

    @Override
    public void run(String... args) throws Exception {
        testStudentService.testTransaction();
    }
}
