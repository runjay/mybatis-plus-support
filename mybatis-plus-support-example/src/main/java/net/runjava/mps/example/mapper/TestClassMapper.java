package net.runjava.mps.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import net.runjava.mps.BaseMapperSupport;
import net.runjava.mps.example.model.TestClass;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:16
 */

@Mapper
public interface TestClassMapper extends BaseMapperSupport<TestClass> {


}
