package net.runjava.mps.example.entity;

import lombok.Data;
import net.runjava.mps.example.model.TestStudent;
import net.runjava.mps.example.model.TestClass;
import net.runjava.mps.query.form.annotation.ConcatType;
import net.runjava.mps.query.form.annotation.Where;
import net.runjava.mps.query.form.annotation.WhereGroup;
import net.runjava.mps.query.form.annotation.WhereGroupItem;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:25
 */


@WhereGroup(value = {
        @WhereGroupItem(name = "a",concatType = ConcatType.OR),
        @WhereGroupItem(name = Where.DEFAULT_GROUP,concatType = ConcatType.AND)
})
@Data
public class QueryStudentForm extends BasePageForm<TestStudent> {

    @Where(type = Where.Type.LIKE_RIGHT,groupName = "a",order = 1)
    private String name;

    @Where(type = Where.Type.BETWEEN,groupName = "a",tableAliasName = "t1",order = 3)
    private Integer [] age;

    @Where(type = Where.Type.IN,groupName = "a",columnName = "id",order = 2,concatType = ConcatType.OR)
    private Long [] ids;

    @Where
    private Long classId;

    @Where(tableClass = TestClass.class,columnName = "code")
    private String classCode;

    /**
     * 未增加where条件，无法自动过滤。 需要手动代码申明
     */
    private String className;

}
