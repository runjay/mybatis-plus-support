package net.runjava.mps.example.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import net.runjava.mps.BaseMapperSupport;
import net.runjava.mps.example.entity.QueryStudentForm;
import net.runjava.mps.example.model.TestStudent;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:16
 */

@Mapper
public interface TestStudentMapper extends BaseMapperSupport<TestStudent> {

    IPage<Map> queryStudent2Map(IPage page, @Param("queryForm") QueryStudentForm form);

    IPage<TestStudent> queryStudent2Model(IPage page, @Param("queryForm") QueryStudentForm form);

    List<TestStudent> queryStudent(@Param("queryForm") QueryStudentForm form);

}
