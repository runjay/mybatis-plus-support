package net.runjava.mps.example.service.impl;

import net.runjava.mps.BaseServiceSupportImpl;
import net.runjava.mps.example.mapper.TestClassMapper;
import net.runjava.mps.example.model.TestClass;
import net.runjava.mps.example.service.TestClassService;
import org.springframework.stereotype.Service;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:22
 */

@Service
public class TestClassServiceImpl extends BaseServiceSupportImpl<TestClassMapper, TestClass> implements TestClassService {



}
