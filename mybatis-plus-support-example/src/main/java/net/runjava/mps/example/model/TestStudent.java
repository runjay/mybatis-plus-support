package net.runjava.mps.example.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:12
 */

@Data
@TableName("test_student")
public class TestStudent {

    @TableId
    private Long id;

    /**
     * 学生名字
     */
    private String name;

    /**
     * 班级ID
     */
    private Long classId;

    /**
     * 年龄
     */
    private Integer age;

}
