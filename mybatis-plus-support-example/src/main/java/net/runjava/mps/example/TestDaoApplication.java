package net.runjava.mps.example;

import net.runjava.mps.starter.spring.boot2.EnabledAutoQueryForm;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 例子请参照 test目录
 * @author Running
 * @version 1.0
 * @since 2024/3/5 13:37
 */

@SpringBootApplication
@EnabledAutoQueryForm
public class TestDaoApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(TestDaoApplication.class);
        application.setWebApplicationType(WebApplicationType.NONE);
        application.run(args);
    }
}
