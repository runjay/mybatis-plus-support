package net.runjava.mps.example.service;

import net.runjava.mps.BaseServiceSupport;
import net.runjava.mps.query.form.extension.QueryFormServiceSupport;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 19:14
 */
public interface BaseService<T> extends QueryFormServiceSupport<T>, BaseServiceSupport<T> {

}
