package net.runjava.mps.example.service;


import net.runjava.mps.example.model.TestClass;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:23
 */
public interface TestClassService extends BaseService<TestClass> {



}
