package net.runjava.mps.example.service;

import net.runjava.mps.example.model.TestStudent;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:22
 */
public interface TestStudentService extends BaseService<TestStudent> {

    void testTransaction();

}
