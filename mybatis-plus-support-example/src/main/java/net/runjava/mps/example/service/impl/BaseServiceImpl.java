package net.runjava.mps.example.service.impl;

import net.runjava.mps.BaseMapperSupport;
import net.runjava.mps.BaseServiceSupportImpl;
import net.runjava.mps.example.service.BaseService;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 19:16
 */
public class BaseServiceImpl<M extends BaseMapperSupport<T>, T> extends BaseServiceSupportImpl<M, T> implements BaseService<T> {


}
