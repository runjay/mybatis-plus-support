# mybatis-plus-support

#### 介绍
mybatis plus 增强框架

- mps-core 提供基础Mapper增强函数  lock  exists incrUpdateById 等函数。查看 [BaseMapperSupportTest.java](mybatis-plus-support-example%2Fsrc%2Ftest%2Fjava%2Fnet%2Frunjava%2Fmps%2Fexample%2Frun%2FBaseMapperSupportTest.java)
- mps-query-form 通用表单查询插件。 查看 [QueryFormTest.java](mybatis-plus-support-example%2Fsrc%2Ftest%2Fjava%2Fnet%2Frunjava%2Fmps%2Fexample%2Frun%2FQueryFormTest.java)

#### 功能说明

TestStudentMapper.xml
```xml
<mapper namespace="net.runjava.mps.example.mapper.TestStudentMapper">
    <sql id="query">
        SELECT
            t1.*,t2.code,t2.class_name
        from test_student t1
        LEFT JOIN test_class t2
        ON(t1.class_id = t2.id )
        WHERE 1 = 1 and 1 = 1 OR 1 = 1
    </sql>
    <select id="queryStudent2Map" resultType="java.util.Map">
        <include refid="query"></include>
        <!-- 额外手动定义条件 -->
        <if test="queryForm.className != null and queryForm.className != ''">
            AND t2.class_name = #{queryForm.className}
        </if>
    </select>
    <select id="queryStudent2Model" resultType="net.runjava.mps.example.model.TestStudent">
        <include refid="query"></include>
    </select>
    <select id="queryStudent" resultType="net.runjava.mps.example.model.TestStudent">
        <include refid="query"></include>
    </select>
</mapper>
```

```java
// 指定分组前后关系以及连接方式，也可以都不设置
@WhereGroup(value = {
        @WhereGroupItem(name = "a",concatType = ConcatType.OR),
        // 默认分组，如不指定则优先默认分组。且所有字段条件都是在最外层不带括弧
        @WhereGroupItem(name = Where.DEFAULT_GROUP,concatType = ConcatType.AND)
})
@Data
public class QueryStudentForm extends BasePageForm<TestStudent> {

    //a 组生成sql   (t1.name LIKE ? OR t1.id IN (?, ?, ?) AND t1.age BETWEEN ? AND ?)
    @Where(type = Where.Type.LIKE_RIGHT,groupName = "a",order = 1)
    private String name;

    @Where(type = Where.Type.BETWEEN,groupName = "a",tableAliasName = "t1",order = 3)
    private Integer [] age;

    @Where(type = Where.Type.IN,groupName = "a",columnName = "id",order = 2,concatType = ConcatType.OR)
    private Long [] ids;

    
    // 默认分组  (t1.class_id = ? AND t2.code = ?)
    @Where
    private Long classId;

    @Where(tableClass = TestClass.class,columnName = "code")
    private String classCode;

    /**
     * 未增加where条件，无法自动过滤。 需要手动mybatis xml代码中定义使用
     */
    private String className;
}


@Mapper
public interface TestStudentMapper extends BaseMapperSupport<TestStudent> {
    IPage<Map> queryStudent2Map(IPage page, QueryStudentForm form);
    IPage<TestStudent> queryStudent2Model(IPage page,QueryStudentForm form);
    List<TestStudent> queryStudent(QueryStudentForm form);
}


```

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class QueryFormTest {


    @Resource
    private TestStudentService testStudentService;

    @Resource
    private TestStudentMapper testStudentMapper;



    /**
     * 单表条件过滤，过滤 QueryStudentForm其他表的字段
     */
    @Test
    public void testSingleTableQuery(){
        QueryStudentForm queryStudentForm = new QueryStudentForm();
        queryStudentForm.setPageNo(1L);
        queryStudentForm.setPageSize(10L);
        // 关联表test_class 字段 code
        queryStudentForm.setClassCode("class1"); 
        queryStudentForm.setAge(new Integer[]{2,31});
        // 只查询主表数据，忽略关联表字段
        IPage page = testStudentService.selectPage(queryStudentForm);
        // 执行sql情况如下：
        // SELECT COUNT(*) AS total FROM test_student WHERE ((age BETWEEN ? AND ?))
        // SELECT id,name,class_id,age FROM test_student WHERE ((age BETWEEN ? AND ?)) LIMIT ?
        System.out.println("总数据条数" + page.getTotal());
    }

    @Test
    public void testMybatisNativeSqlQuery(){
        QueryStudentForm queryStudentForm = new QueryStudentForm();
        queryStudentForm.setName("李"); //姓李的
        queryStudentForm.setPageNo(1L);
        queryStudentForm.setPageSize(10L);
        queryStudentForm.setAge(new Integer[]{9,13});
        queryStudentForm.setClassId(1L);
        queryStudentForm.setClassCode("class1");
        queryStudentForm.setIds(new Long[]{1L,3L,5L});
        queryStudentForm.setClassName("1班");
        queryStudentForm.setSortBy("id");
        queryStudentForm.setSortType("desc");
 
        IPage page1 = testStudentMapper.queryStudent2Map(queryStudentForm.buildPage(),queryStudentForm);
        //  sql语句执行如下    
        //  SELECT COUNT(*) AS total FROM test_student t1 LEFT JOIN test_class t2 ON (t1.class_id = t2.id) WHERE 1 = 1   注:原生语句部分
        //  OR (t1.name LIKE ? OR t1.id IN (?, ?, ?) AND t1.age BETWEEN ? AND ?) AND (t1.class_id = ? AND t2.code = ?)   注:表单查询插件自动生成部分
        
        //  SELECT t1.*, t2.code, t2.class_name FROM test_student t1 LEFT JOIN test_class t2 ON (t1.class_id = t2.id) WHERE 1 = 1               注:原生语句部分
        //  OR (t1.name LIKE ? OR t1.id IN (?, ?, ?) AND t1.age BETWEEN ? AND ?) AND (t1.class_id = ? AND t2.code = ?) ORDER BY id DESC LIMIT ? 注:表单查询插件自动生成部分
    }
}
```



#### 使用说明

spring maven 依赖 (不包含mybatis-plus依赖)
```
<dependency>
    <groupId>net.runjava.mps</groupId>
    <artifactId>mybatis-plus-support-query-form</artifactId>
    <version>1.0.1-beta</version>
</dependency>
```

```java

import net.runjava.mps.injector.MapperSupportSqlInjector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MpsConfig {

    @Bean
    public MapperSupportSqlInjector mapperSupportSqlInjector(){
        /**
         * mybatis-plus增强操作
         */
        return new MapperSupportSqlInjector();
    }

    @Bean
    public MybatisPlusInterceptor baseMybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // queryForm 拦截器必须在分页插件之前，要不然会导致参数顺序错乱
        interceptor.addInnerInterceptor(new QueryFormInterceptor());
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }
    
}

```

spring boot2 maven依赖

```
<dependency>
    <groupId>net.runjava.mps</groupId>
    <artifactId>mybatis-plus-support-spring-boot2-starter</artifactId>
    <version>1.0.1-beta</version>
</dependency>
```

spring boot 3 maven依赖
```
<dependency>
    <groupId>net.runjava.mps</groupId>
    <artifactId>mybatis-plus-support-spring-boot2-starter</artifactId>
    <version>1.0.1-beta</version>
</dependency>
```

spring boot 启用表单查询插件
```java
@SpringBootApplication
// 启用表单查询插件
@EnabledAutoQueryForm
public class Application {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        application.run(args);
    }
}
```
