package net.runjava.mps.query.form.annotation;

import java.lang.annotation.*;

/**
 * 用于定义Where条件分组之后的前后顺序以及连接方式
 * <p>
 * 在不指定分组顺序时，优先使用默认分组的条件
 * @author Running
 * @version 1.0
 */
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WhereGroup {
   WhereGroupItem [] value() default {};

}
