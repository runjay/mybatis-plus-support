package net.runjava.mps.query.form.annotation;

import java.lang.annotation.*;

/**
 * @author Running
 * @version 1.0
 */

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WhereGroupItem {

    /**
     * 分组的名称
     */
    String name();

    /**
     * 跟上个分组之间的连接关系
     * <p>
     * 若分组为第一个分组且 原生sql where不包含任何条件，则不起任何作用
     */
    ConcatType concatType() default ConcatType.AND;

}
