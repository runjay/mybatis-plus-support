package net.runjava.mps.query.form.annotation;

import java.lang.annotation.*;

/**
 * @author Running
 * @version 1.0
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Where {


    /**
     * 默认分组名称
     */
    String DEFAULT_GROUP = "DEFAULT_GROUP";


    /**
     * 数据库对应的列名
     *  <p>
     * 若未申明，则根据字段属性名，且驼峰自动转成下划线
     */
    String columnName() default "";

    /**
     * 分组名称
     */
    String groupName() default DEFAULT_GROUP;


    /**
     * 跟上个字段之间的连接方式
     * <p>
     * 若字段为第一个条件字段则不起任何作用
     */
    ConcatType concatType() default ConcatType.AND;


    /**
     * 字段排序的先后顺序 越小越靠前
     */
    int order() default Integer.MAX_VALUE;


    /**
     * 表对应的model,多表关联时需要申明。
     * <p>
     * 默认是QueryForm的申明的泛型值
     */
    Class tableClass() default Void.class;


    /**
     *  当tableAliasName非空数组时，在tableClass跟别名都匹配时才会加入过滤条件
     * <p>
     * 在一个查询中，同一张表自关联或者出现多次时。若要让条件作用某一张表时，必须申名子表名称
     * <p>
     * 例如： select t1.*
     * <p>
     *          form sys_menu t1
     * <p>
     *          let join sys_menu t2 on(t1.parent_id = t2.id )
     * <p>
     *        where t1.status = 1 and t2.status = 1
     * <p>
     *   此时如要根据菜单t1菜单名称过滤时，则需要申明 tableAliasName
     */
    String tableAliasName() default "";

    /**
     * 过滤方式
     */
    Type type() default Type.EQ;


    enum Type {
        /**
         * 等于
         */
        EQ,
        /**
         * 不等于
         */
        NE,
        /**
         * 大于等于
         */
        GE,
        /**
         * 大于
         */
        GT,

        /**
         * 小于等于
         */
        LE,
        /**
         * 小于
         */
        LT,

        /**
         * 全模糊 like '%abc%'
         */
        LIKE,
        /**
         * 左模糊 like '%abc'
         */
        LIKE_LEFT,

        /**
         * 右模糊 like 'abc%'
         */
        LIKE_RIGHT,

        /**
         *  where a in (1,2,4)
         */
        IN,

        /**
         * where a not in (1,2,4)
         */
        NOT_IN,
        /**
         * where a between(1,19)
         */
        BETWEEN,

        /**
         * 非空
         */
        NOT_NULL,
        /**
         * 判空
         */
        IS_NULL,

    }

}
