package net.runjava.mps.query.form.annotation;

/**
 * SQL条件之间的关联方式
 * @author Running
 * @version 1.0
 * @since 2024-03-05
 */
public enum ConcatType {

    AND,

    OR

}
