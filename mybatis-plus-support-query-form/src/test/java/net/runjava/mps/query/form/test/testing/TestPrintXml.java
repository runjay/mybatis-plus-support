package net.runjava.mps.query.form.test.testing;

import net.runjava.mps.query.form.test.form.QueryStudentForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Running
 * @since 2024/3/18 20:39
 */


@RunWith(SpringRunner.class)
@SpringBootTest
public class TestPrintXml {

    @Test
    public void test(){
        QueryStudentForm queryStudentForm = new QueryStudentForm();
        queryStudentForm.setName("李");
        queryStudentForm.setPageNo(1L);
        queryStudentForm.setPageSize(10L);
        queryStudentForm.setAge(new Integer[]{10,11});
        queryStudentForm.printMybatisWhereXml();
    }

}
