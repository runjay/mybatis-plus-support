package net.runjava.mps.query.form.test;

import net.runjava.mps.query.form.test.config.EnabledAutoQueryForm;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.TestPropertySource;

/**
 * @author Running
 * @since 2024/3/18 20:49
 */
@SpringBootApplication
@EnabledAutoQueryForm
@TestPropertySource
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(TestApplication.class);
        application.setWebApplicationType(WebApplicationType.NONE);
        application.run(args);
    }

}
