package net.runjava.mps.query.form.test.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:14
 */


@Data
@TableName("test_class")
public class TestClass {

    @TableId
    private Long id;

    /**
     * 班级名字
     */
    private String className;

    /**
     * 班级编码
     */
    private String code;

}
