package net.runjava.mps.query.form.test.mapper;

import net.runjava.mps.BaseMapperSupport;
import net.runjava.mps.query.form.test.model.TestStudent;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/12 17:16
 */

@Mapper
public interface TestStudentMapper extends BaseMapperSupport<TestStudent> {


}
