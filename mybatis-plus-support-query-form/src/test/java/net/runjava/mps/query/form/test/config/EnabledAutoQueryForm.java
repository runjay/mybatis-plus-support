package net.runjava.mps.query.form.test.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 17:57
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({MpsQueryFormAutoConfig.class})
public @interface EnabledAutoQueryForm {


}
