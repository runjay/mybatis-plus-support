package net.runjava.mps.query.form.metadata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import net.runjava.mps.exception.MpsException;
import net.runjava.mps.query.form.annotation.Where;
import net.runjava.mps.utils.ColumnCacheUtils;

import java.lang.reflect.Field;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/6 20:47
 */

public class WhereField implements Comparable<WhereField>{

    public WhereField(Field field,Where where,Class mainModelClass){
        this.field = field;
        this.where = where;
        init(mainModelClass);
    }

    private void init(Class mainModelClass){
        if(Void.class == where.tableClass()){
            this.tableClass = mainModelClass;
        }else{
            this.tableClass = where.tableClass();
        }

        Where where = this.getWhere();
        String columnName = where.columnName();
        if(StringUtils.isNotBlank(columnName)){
            this.columnName = columnName;
        }else{
            String fieldName = field.getName();
            columnName = ColumnCacheUtils.getColumn(this.tableClass,fieldName);
            if(StringUtils.isNotBlank(columnName)){
                this.columnName =  columnName;
            }
        }

        TableName tableName = (TableName)this.tableClass.getAnnotation(TableName.class);
        if(tableName == null){
            throw new MpsException("Class :[" + this.tableClass.getName() + "] is not a mapper model ");
        }
        this.tableName = tableName.value().trim().toLowerCase();
    }

    private Field field;

    private Where where;

    /**
     * 数据库字段名称
     */
    private String columnName;

    /**
     *  where 字段所属于的表名
     */
    private String tableName;

    /**
     * model类对应class
     */
    private Class tableClass;

    public String getGroupName(){
        return this.where.groupName();
    }

    public boolean isDefaultGroup(){
        return Where.DEFAULT_GROUP.equals(this.where.groupName());
    }

    @Override
    public int compareTo(WhereField o) {
        return Integer.compare(this.getWhere().order(),o.getWhere().order());
    }


    public Field getField() {
        return field;
    }

    public Where getWhere() {
        return where;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public Class getTableClass() {
        return tableClass;
    }
}
