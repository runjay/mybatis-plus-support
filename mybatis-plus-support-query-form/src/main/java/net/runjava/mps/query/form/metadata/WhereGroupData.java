package net.runjava.mps.query.form.metadata;

import net.runjava.mps.query.form.annotation.ConcatType;

import java.util.List;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 13:59
 */

public class WhereGroupData {

    public WhereGroupData(String groupName,ConcatType concatType,List<WhereField> items,boolean outside){
        this.groupName = groupName;
        this.concatType = concatType;
        this.items = items;
        this.outside = outside;
    }

    public WhereGroupData(String groupName,ConcatType concatType,List<WhereField> items){
        this.groupName = groupName;
        this.concatType = concatType;
        this.items = items;
        this.outside = false;
    }

    /**
     * 最外层，不增加括号
     */
    private boolean outside;

    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 分组连接方式
     */
    private ConcatType concatType;

    /**
     * where字段条件集合
     */
    private List<WhereField> items;


    public String getGroupName() {
        return groupName;
    }

    public ConcatType getConcatType() {
        return concatType;
    }

    public List<WhereField> getItems() {
        return items;
    }

    public boolean isOutside() {
        return outside;
    }
}
