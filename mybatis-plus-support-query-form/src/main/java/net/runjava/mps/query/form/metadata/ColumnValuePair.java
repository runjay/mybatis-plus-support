package net.runjava.mps.query.form.metadata;

/**
 * @author Running
 * @since 2024/3/18 17:30
 */
public class ColumnValuePair {

    public ColumnValuePair(WhereField column,Object value,WhereGroupData groupInfo){
        this.column = column;
        this.value = value;
        this.groupInfo = groupInfo;
    }

    private WhereGroupData groupInfo;

    private WhereField column;

    private Object value;


    public WhereField getColumn() {
        return column;
    }

    public Object getValue() {
        return value;
    }


    public WhereGroupData getGroupInfo() {
        return groupInfo;
    }
}
