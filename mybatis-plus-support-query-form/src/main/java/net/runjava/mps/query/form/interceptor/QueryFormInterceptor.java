package net.runjava.mps.query.form.interceptor;

import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import net.runjava.mps.query.form.QueryForm;
import net.runjava.mps.query.form.SqlQueryProcessor;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;


/**
 * 用于实现原生mybatis sql查询自动追加 where 过滤条件
 * 必须申明在分页插件之前 {@link com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor}
 * @author Running
 * @version 1.0
 * @since 2024/3/7 13:21
 */
public class QueryFormInterceptor implements InnerInterceptor {


    /**
     * 无需进行拦截的函数
     */
    private final static Set<String> IGNORE_METHODS = new ConcurrentSkipListSet<>();


    @Override
    public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql)
            throws SQLException {

        if (ms.getSqlCommandType() != SqlCommandType.SELECT) {
            return;
        }

        if(IGNORE_METHODS.contains(ms.getId())){
            return;
        }

        QueryForm queryForm = findForm(parameter);
        if(queryForm == null){
            IGNORE_METHODS.add(ms.getId());
            return;
        }
        SqlQueryProcessor.process(ms,boundSql,queryForm,parameter);
    }

    private QueryForm findForm(Object parameterObject) {
        if (parameterObject != null) {
            if (parameterObject instanceof Map) {
                Map<Object, Object> parameterMap = (Map)parameterObject;
                Iterator var2 = parameterMap.entrySet().iterator();

                while(var2.hasNext()) {
                    Map.Entry entry = (Map.Entry)var2.next();
                    if (entry.getValue() != null && entry.getValue() instanceof QueryForm) {
                        QueryForm queryForm = (QueryForm)entry.getValue();
                        if(queryForm.autoAppendNativeSql()){
                            return queryForm;
                        }
                    }
                }
            } else if (parameterObject instanceof QueryForm) {
                return (QueryForm)parameterObject;
            }
        }
        return null;
    }


}
