package net.runjava.mps.query.form;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.runjava.mps.query.form.annotation.ConcatType;
import net.runjava.mps.query.form.annotation.WhereGroup;
import net.runjava.mps.query.form.annotation.WhereGroupItem;
import net.runjava.mps.query.form.metadata.*;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import net.runjava.mps.exception.MpsException;
import net.runjava.mps.query.form.annotation.Where;
import net.runjava.mps.utils.ArrayUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/6 19:55
 */
public abstract class AbstractQueryForm<T> implements QueryForm<T> {

    protected final Log log = LogFactory.getLog(this.getClass());

    /**
     * {@link Where} 注解字段缓存
     */
    private static final Map<Class, List<WhereGroupData>> CLASS_WHERE_FIELDS_CACHE = new ConcurrentHashMap<>(256);


    /**
     * @return 构建单表查询条件
     */
    @Override
    public QueryWrapper<T> buildQuery() {
        QueryWrapper<T> queryWrapper = new QueryWrapper();
        List<WhereGroupColumnValueData> valueColumnGroups = getQueryColumnValues(true);

        for (int i = 0; i < valueColumnGroups.size(); i++) {
            WhereGroupColumnValueData columnValueData = valueColumnGroups.get(i);
            List<ColumnValuePair> pairsGroup = columnValueData.getValuePairs();
            int itemSize = pairsGroup.size();
            if(itemSize == 0){
                continue;
            }

            if(columnValueData.isOutside()){
                // 最外层
                appendCondition(queryWrapper,pairsGroup);
            }else{
                if(columnValueData.getConcatType() == ConcatType.AND){
                    // 组与组之间用AND 关联
                    queryWrapper.and(new Consumer<QueryWrapper<T>>() {
                        @Override
                        public void accept(QueryWrapper<T> tQueryWrapper) {
                            appendCondition(tQueryWrapper,pairsGroup);
                        }
                    });
                }else if(columnValueData.getConcatType() == ConcatType.OR){
                    // 组与组之间用OR 关联
                    queryWrapper.or(new Consumer<QueryWrapper<T>>() {
                        @Override
                        public void accept(QueryWrapper<T> tQueryWrapper) {
                            appendCondition(tQueryWrapper,pairsGroup);
                        }
                    });
                }
            }
        }
        return queryWrapper;
    }




    /**
     * @param onlyMainTable 过滤非主表的字段
     * @return 获取需要参与过滤的字段集合
     */
    private List<WhereGroupColumnValueData> getQueryColumnValues(boolean onlyMainTable){
        List<WhereGroupData> groupDataList = this.getGroupFields();
        int size = groupDataList.size();
        List<WhereGroupColumnValueData> result = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            WhereGroupData groupData = groupDataList.get(i);
            List<WhereField> items = groupData.getItems();
            int groupItems = items.size();

            List<ColumnValuePair> valuePairs = new ArrayList<>();
            WhereGroupColumnValueData columnValues = new WhereGroupColumnValueData(groupData,valuePairs);
            for (int j = 0; j < groupItems; j++) {
                WhereField whereField = items.get(j);
                Object value = this.getFieldValue(whereField,onlyMainTable);
                if(value != null){
                    valuePairs.add(new ColumnValuePair(whereField,value,groupData));
                }
            }
            if(valuePairs.size() > 0){
                result.add(columnValues);
            }
        }
        return result;
    }

    /**
     *  获取字段对应的属性值并做加工处理
     * @param whereField
     *           字段
     * @param onlyMainTable
     *          过滤非主表的字段
     * @return  获取字段对应的值
     */
    private Object getFieldValue(WhereField whereField,boolean onlyMainTable) {
        Field field = whereField.getField();

        if(onlyMainTable && whereField.getTableClass() != this.getModelClass()){
            return null;
        }

        Object value = null;
        try {
            value = ReflectionKit.getFieldValue(this, field.getName());
        } catch (Exception e) {
            log.error("Get class [" + this.getClass().getName() + "] property [" + field.getName() + "] error : " + e.getMessage());
            throw new MpsException("Class [" + this.getClass().getName() + "] do get property [" + field.getName() + "] failure ");
        }

        if (value == null) {
            return null;
        }

        if (value instanceof String && StringUtils.isBlank((String) value)) {
            return null;
        }

        if (value instanceof Collection && CollectionUtils.isEmpty( (Collection)value)){
            return null;
        }

        if(ArrayUtils.isArray(value)){
            // 数组转换成List统一处理
            value = ArrayUtils.toList(value);
        }

        Where.Type type = whereField.getWhere().type();
        if(type == Where.Type.BETWEEN){
            if(value instanceof List){
                List params = (List)value;
                if(params.size() >= 2){
                    return params;
                }
                return null;
            }
            throw new MpsException("Query Form class [" + this.getClass().getName() + "] property [" + field.getName() + "] use filter type is [BETWEEN],But it isn't an array");
        }
        return value;
    }

    @Override
    public List<WhereGroupColumnValueData> buildNativeQueryFormData() {
        return this.getQueryColumnValues(false);
    }

    /**
     * @return 分页参数
     */
    @Override
    public IPage buildPage(){
        long pageSize = this.getPageSize();
        long maxPageSize = getMaxPageSize();
        if(pageSize > maxPageSize){
            pageSize = maxPageSize;
        }
        Page page = new Page(this.getPageNo(), pageSize);
        List<OrderItem> orders = this.getOrders();
        if(orders == null){
            // 当前端未下发排序方式，则获取默认排序方式
            orders = this.getDefaultOrders();
        }
        if(orders != null){
            page.setOrders(orders);
        }
        return page;
    }

    private List<WhereGroupData> buildWhereGroupData(){
        Class clazz = getClass();
        Map<String,List<WhereField>> groupMap = new HashMap<>();
        Field [] fields = clazz.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            WhereField whereField = buildWhereField(field);
            if(whereField == null){
                continue;
            }
            Where where = whereField.getWhere();
            String groupName = where.groupName();

            List<WhereField> fieldList = groupMap.get(groupName);
            if(fieldList == null){
                fieldList = new ArrayList<>();
                groupMap.put(groupName,fieldList);
            }
            fieldList.add(whereField);
        }
        List<WhereGroupData> result = new ArrayList<>();
        WhereGroup whereGroup = (WhereGroup)clazz.getAnnotation(WhereGroup.class);
        LinkedHashMap<String,WhereGroupItem> groupOrdersMap = getGroupOrderList(whereGroup);
        Set<String> alreadyAdd = new HashSet<>();
        if(!groupOrdersMap.containsKey(Where.DEFAULT_GROUP)){
            // 未指定默认分组的情况下，优先使用默认分组的条件。且暴露在最外层
            this.addWhereGroupData(result,alreadyAdd,groupMap,Where.DEFAULT_GROUP,ConcatType.AND,true);
        }

        // 添加QueryForm注解上指定的分组顺序
        for (String groupName : groupOrdersMap.keySet()) {
            WhereGroupItem whereGroupItem = groupOrdersMap.get(groupName);
            ConcatType concatType = whereGroupItem == null ? ConcatType.AND : whereGroupItem.concatType();
            this.addWhereGroupData(result,alreadyAdd,groupMap,groupName,concatType,false);
        }

        // 补充添加QueryForm注解上未指定的分组
        for (String groupName : groupMap.keySet()){
            this.addWhereGroupData(result,alreadyAdd,groupMap,groupName,ConcatType.AND,false);
        }
        return result;
    }


    private void addWhereGroupData(List<WhereGroupData> result,Set<String> alreadyAdd,Map<String,List<WhereField>> groupMap,String groupName,ConcatType concatType,boolean outside){
        if(alreadyAdd.contains(groupName)){
            return;
        }
        List<WhereField> fieldList = groupMap.get(groupName);
        if(CollectionUtils.isEmpty(fieldList)){
            return;
        }
        // 根据Where上配置的order属性排序字段
        Collections.sort(fieldList);
        WhereGroupData whereGroupData = new WhereGroupData(groupName,concatType,Collections.unmodifiableList(fieldList),outside);
        result.add(whereGroupData);
        alreadyAdd.add(groupName);
    }


    private LinkedHashMap<String,WhereGroupItem> getGroupOrderList(WhereGroup whereGroup){
        LinkedHashMap<String,WhereGroupItem> groupMap = new LinkedHashMap<>();
        if(whereGroup == null){
            return groupMap;
        }
        WhereGroupItem [] groupItems = whereGroup.value();
        for (int i = 0; i < groupItems.length; i++) {
            WhereGroupItem item = groupItems[i];
            groupMap.put(item.name(),item);
        }
        return groupMap;
    }



    /**
     * @return 获取分组之后的字段集合
     */
    private List<WhereGroupData> getGroupFields(){
        Class clazz = getClass();
        List<WhereGroupData> result = CLASS_WHERE_FIELDS_CACHE.get(clazz);
        if(result != null){
            return result;
        }

        synchronized (clazz){
            result = Collections.unmodifiableList(this.buildWhereGroupData());
            CLASS_WHERE_FIELDS_CACHE.put(clazz,result);
            return result;
        }
    }




    private WhereField buildWhereField(Field field){
        Where where  = field.getAnnotation(Where.class);
        if(where == null){
            return null;
        }
        WhereField whereField = new WhereField(field,where,this.getModelClass());
        return whereField;
    }


    private boolean isEmpty(Object value){
        if(value == null){
            return true;
        }

        if(value instanceof String && StringUtils.isBlank((String)value)){
            return true;
        }

        if(value instanceof Collection &&  ((Collection)value).isEmpty()){
           return true;
        }
        return false;
    }

    private void appendCondition(QueryWrapper queryWrapper,List<ColumnValuePair> pairs){
        for (int j = 0; j < pairs.size(); j++) {
            ColumnValuePair pair = pairs.get(j);
            WhereField whereField = pair.getColumn();
            Where where = whereField.getWhere();
            if(where.concatType() == ConcatType.OR){
                // 追加or条件
                queryWrapper.or();
            }
            this.appendCondition(queryWrapper,pair);
        }
    }

    private void appendCondition(QueryWrapper queryWrapper,ColumnValuePair pair){
        WhereField whereField = pair.getColumn();
        Object value = pair.getValue();
        String column = whereField.getColumnName();
        Where where = whereField.getWhere();
        Where.Type type = where.type();
        if(type == null){
            return;
        }

        switch (type){
            case EQ:
                queryWrapper.eq(column,value);
                break;
            case BETWEEN:
                if(!(value instanceof List)){
                    Field field = whereField.getField();
                    throw new MpsException("Class :[" + this.getClass() + "] property [" + field.getName() + "] type is not a List ");
                }
                List data = (List)value;
                if(data.size() < 2){
                    break;
                }
                Object start = data.get(0);
                Object end = data.get(1);
                if(start == null || end == null){
                    break;
                }
                queryWrapper.between(column,start,end);
                break;
            case GT:
                queryWrapper.gt(column,value);
                break;
            case GE:
                queryWrapper.ge(column,value);
                break;
            case LT:
                queryWrapper.lt(column,value);
                break;
            case LE:
                queryWrapper.le(column,value);
                break;
            case LIKE:
                queryWrapper.like(column,value);
                break;
            case LIKE_RIGHT:
                queryWrapper.likeRight(column,value);
                break;
            case LIKE_LEFT:
                queryWrapper.likeLeft(column,value);
                break;
            case IS_NULL:
                if(Objects.equals(value,Boolean.TRUE)){
                    queryWrapper.isNull(column);
                }
                break;
            case NOT_NULL:
                if(Objects.equals(value,Boolean.TRUE)){
                    queryWrapper.isNotNull(column);
                }
                break;
            case NOT_IN:
                this.checkCollection(value,whereField);
                queryWrapper.notIn(column,(Collection) value);
                break;
            case IN:
                this.checkCollection(value,whereField);
                queryWrapper.in(column,(Collection) value);
                break;
        }
    }


    /**
     * 若开发者不想使用自动注入sql方式，可以手动调用该函数生成并输出mybatis where条件到控制台并复制到对应的XML文件中去
     */
    @Override
    public void printMybatisWhereXml() {
        printMybatisWhereXml("queryForm");
    }

    @Override
    public void printMybatisWhereXml(String queryFormParamName) {
        new QueryFormXmlPrinter(this.getGroupFields(),this).printWhereXml(queryFormParamName);
    }

    private void checkCollection(Object value, WhereField whereField){
        Field field = whereField.getField();
        if(!(value instanceof Collection)){
            throw new MpsException("Class :[" + this.getClass() + "] property [" + field.getName() + "] type is not a Collection ");
        }
    }

    private Class<T> getModelClass(){
        Type type = getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return (Class<T>) parameterizedType.getActualTypeArguments()[0];
        }
        // QueryForm未填写Model泛型
        throw new MpsException("Query form :[" + this.getClass() + "] missing generic super model class ");
    }


    /**
     * @return 返回单页最大查询条数，防止客户端一次性请求过多数据
     */
    public long getMaxPageSize(){
        return 1000;
    }

    /**
     * @return 一页数据条数
     */
    public abstract long getPageSize();


    /**
     * @return 分页页码
     */
    public abstract long getPageNo();


    /**
     * @return 排序方式
     */
    public abstract List<OrderItem> getOrders();

    /**
     * @return 默认排序方式
     */
    public abstract List<OrderItem> getDefaultOrders();

}
