package net.runjava.mps.query.form;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import net.runjava.mps.query.form.annotation.ConcatType;
import net.runjava.mps.query.form.metadata.BuildSqlData;
import net.runjava.mps.query.form.metadata.ColumnValuePair;
import net.runjava.mps.query.form.metadata.WhereField;
import net.runjava.mps.query.form.metadata.WhereGroupColumnValueData;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.*;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.scripting.defaults.RawSqlSource;
import net.runjava.mps.exception.MpsException;
import net.runjava.mps.query.form.annotation.Where;
import net.runjava.mps.utils.ArrayUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/7 16:32
 */

public class SqlQueryProcessor {


    private static final Log log = LogFactory.getLog(SqlQueryProcessor.class);

    /**
     * sql缓存
     */
    private static Map<String,SqlCache> SQL_CACHE_MAP = new ConcurrentHashMap<>();


    /**
     * 用于动态替换追加条件
     */
    private static final String DEFAULT_WHERE_REPLACE_KEY = "#{esb.where.replace}";

    private static final String DEFAULT_QUERY_FORM_KEY = "queryForm";


    public static void process(MappedStatement ms, BoundSql boundSql, QueryForm queryForm,Object parameter){
        List<WhereGroupColumnValueData> dataGroups = queryForm.buildNativeQueryFormData();
        if(CollectionUtils.isEmpty(dataGroups)){
            return;
        }
        // 获取并构建sql缓存
        SqlCache sqlCache = getSqlCache(ms.getId(), boundSql,ms);

        PluginUtils.MPBoundSql mpBoundSql = PluginUtils.mpBoundSql(boundSql);
        Map<String, Object> additionalParameter = mpBoundSql.additionalParameters();

        String queryFormKey = createQueryFormKey(additionalParameter);
        additionalParameter.put(queryFormKey,queryForm);

        List<ParameterMapping> parameterMappings = mpBoundSql.parameterMappings();
        BuildSqlData buildSqlData = new BuildSqlData();
        buildSqlData.setMpBoundSql(mpBoundSql);
        buildSqlData.setQueryFormKey(queryFormKey);
        buildSqlData.setMappedStatement(ms);
        buildSqlData.setParameterMappings(parameterMappings);


        // 生成要追加的sql
        String whereAppend = buildSql(sqlCache,dataGroups,buildSqlData);


        WhereGroupColumnValueData firstGroup= dataGroups.get(0);
        ConcatType concatType = firstGroup.getConcatType();

        String sqlTemplate = concatType == ConcatType.OR ? sqlCache.getOrSql() : sqlCache.getAndSql();
        // 将占位符替换成要追加的sql
        String newString = sqlTemplate.replace(sqlCache.getWhereReplaceKey(),whereAppend);

        mpBoundSql.parameterMappings(parameterMappings);
        mpBoundSql.sql(newString);
    }

    private static String createQueryFormKey(Map<String, Object> additionalParameter){
        int index = 0;
        do{
           String key = index == 0 ?  DEFAULT_QUERY_FORM_KEY : DEFAULT_QUERY_FORM_KEY + index;
           if(!additionalParameter.containsKey(key)){
               return key;
           }
           index++;
        }while(true);
    }


    private static String buildSql(SqlCache sqlCache,List<WhereGroupColumnValueData> dataGroups,BuildSqlData buildSqlData){
        // List<WhereGroupColumnValueData> dataGroups
        StringBuffer sqlBuffer = new StringBuffer();

        int paramsSize = dataGroups.size();
        for (int i = 0; i < paramsSize; i++) {
            WhereGroupColumnValueData valueData = dataGroups.get(i);
            if(i != 0 ){
                // 增加分组条件
                sqlBuffer.append(" " + valueData.getConcatType().name() + " ");
            }

            boolean outside = valueData.isOutside();
            List<ColumnValuePair> pairs = valueData.getValuePairs();

            if(!outside){
                // 增加分组条件括弧开始
                sqlBuffer.append("(");
            }

            for (int j = 0; j < pairs.size(); j++) {
                ColumnValuePair pair = pairs.get(j);
                WhereField whereField = pair.getColumn();
                Where where = whereField.getWhere();
                if(j != 0){
                    sqlBuffer.append(" " + where.concatType().name() + " ");
                }

                sqlBuffer.append(buildFieldSql(sqlCache,pair,buildSqlData));
            }

            if(!outside){
                // 增加分组条件括弧结束
                sqlBuffer.append(")");
            }
        }
        return sqlBuffer.toString();
    }


    private static String buildFieldSql(SqlCache sqlCache,ColumnValuePair pair,BuildSqlData buildSqlData){

        WhereField whereField = pair.getColumn();
        Where where = whereField.getWhere();
        Field field = whereField.getField();

        String columnName = whereField.getColumnName();
        String tableName = whereField.getTableName();
        String tableAliasName = where.tableAliasName();

        if(StringUtils.isBlank(tableAliasName)){
            if(sqlCache.getRepeatTableSet().contains(tableName)){
                throw new MpsException("The table [" + tableName + "] has appeared more than 2 times in sql,Pleas set table alias name on query form");
            }else{
                String aliasName = sqlCache.getTableNameMap().get(tableName);
                if(StringUtils.isNotBlank(aliasName)){
                    tableAliasName = aliasName;
                }else{
                    tableAliasName = tableName;
                }
            }
        }

        String queryFormKey = buildSqlData.getQueryFormKey();

        StringBuffer sqlBuffer = new StringBuffer(tableAliasName + "." + columnName);

        Object value = pair.getValue();
        String key = queryFormKey + "." + field.getName();
        switch (where.type()){
            case EQ:
                sqlBuffer.append(" = ? ");
                addParameter(buildSqlData,key, value);
                break;
            case BETWEEN:
                List list = (List)value;
                Map<String, Object> additionalParameter = buildSqlData.getMpBoundSql().additionalParameters();
                String itemStartKey = buildCollectionParameterKey(key,0);
                String itemEndKey = buildCollectionParameterKey(key,1);
                addParameter(buildSqlData,itemStartKey, list.get(0));
                additionalParameter.put(itemStartKey,list.get(0));

                addParameter(buildSqlData,itemEndKey, list.get(1));
                additionalParameter.put(itemEndKey,list.get(1));
                sqlBuffer.append(" BETWEEN ? AND ?");
                break;
            case GT:
                sqlBuffer.append(" > ?");
                addParameter(buildSqlData,key, value);
                break;
            case GE:
                sqlBuffer.append(" >= ?");
                addParameter(buildSqlData,key, value);
                break;
            case LT:
                sqlBuffer.append(" < ?");
                addParameter(buildSqlData,key, value);
                break;
            case LE:
                sqlBuffer.append(" <= ?" );
                addParameter(buildSqlData,key, value);
                break;
            case LIKE:
                sqlBuffer.append(" LIKE ?");
                addLikeParameter(buildSqlData,key, "%"+value+"%");
                break;
            case LIKE_RIGHT:
                sqlBuffer.append(" LIKE ?");
                addLikeParameter(buildSqlData,key, "%"+value);
                break;
            case LIKE_LEFT:
                sqlBuffer.append(" LIKE ?");
                addLikeParameter(buildSqlData,key, value+"%");
                break;
            case IS_NULL:
                sqlBuffer.append(" IS NULL");
                break;
            case NOT_NULL:
                sqlBuffer.append(" IS NOT NULL");
                break;
            case NOT_IN:
                sqlBuffer.append(" NOT IN(");
                addCollectionParameter(sqlBuffer,buildSqlData,key, (Collection)value);
                sqlBuffer.append(")");
                break;
            case IN:
                sqlBuffer.append(" IN (");
                addCollectionParameter(sqlBuffer,buildSqlData,key, (Collection)value);
                sqlBuffer.append(")");
                break;
        }
        return sqlBuffer.toString();
    }

    private static void addLikeParameter(BuildSqlData buildSqlData,String key, Object value){
        String newKey = key.replace(StringPool.DOT,StringPool.AT);
        Map<String, Object> additionalParameter = buildSqlData.getMpBoundSql().additionalParameters();
        additionalParameter.put(newKey,value);
        addParameter(buildSqlData,newKey, value);
    }

    private static void addCollectionParameter(StringBuffer sql, BuildSqlData buildSqlData,String key, Object array) {
        Collection collection = null;
        if(array instanceof Collection){
            collection = (Collection)array;
        }else{
            collection = ArrayUtils.toList(array);
        }
        //String newKey = key.replace(StringPool.DOT,StringPool.AT);
        Map<String,Object> additionalParameters = buildSqlData.getMpBoundSql().additionalParameters();
        int index = 0;
        for (Iterator it = collection.iterator(); it.hasNext();) {

            if(index != 0){
                sql.append(StringPool.COMMA);
            }
            sql.append(StringPool.QUESTION_MARK);
            Object itemValue = it.next();
            String itemKey = buildCollectionParameterKey(key,index);
            additionalParameters.put(itemKey,itemValue);
            addParameter(buildSqlData,itemKey, itemValue);
            index++;
        }
    }

    /**
     * 生成数据参数的key值
     * @param key
     * @param index
     * @return
     */
    private static String buildCollectionParameterKey(String key,int index){
        return key.replace(StringPool.DOT,StringPool.AT)  + StringPool.AT + index;
    }

    private static void addParameter(BuildSqlData buildSqlData,String key, Object value){
        List<ParameterMapping> parameterMappings = buildSqlData.getParameterMappings();
        MappedStatement ms = buildSqlData.getMappedStatement();
        parameterMappings.add(new ParameterMapping.Builder(ms.getConfiguration(),key,value.getClass()).build());
    }


    private static SqlCache getSqlCache(String methodId, BoundSql boundSql,MappedStatement ms){
        SqlCache sqlCache = SQL_CACHE_MAP.get(methodId);
        if(sqlCache != null){
            return sqlCache;
        }
        try {
            sqlCache = buildSqlCache(methodId, boundSql);
            if(ms.getSqlSource() instanceof RawSqlSource){
                // 只缓存静态sql
                SQL_CACHE_MAP.put(methodId,sqlCache);
            }
        }catch (Exception e){
            throw new MpsException("method: [" + methodId + "] query form process error:" + e.getMessage());
        }
        return sqlCache;
    }

    /**
     *  只支持普通sql查询如下列
     *  SELECT FROM table t1
     *      JOIN table2 t2  ON( ... )
     *      JOIN table3 t3  ON( ... )
     *      JOIN (SELECT * FROM table4 WHERE a = 1)  t4 ON(...)
     *   WHERE t1.id = 1
     *
     *
     * @param boundSql
     * @param methodId Mapper对应的函数id
     */
    private static SqlCache buildSqlCache(String methodId, BoundSql boundSql) throws JSQLParserException {
        String sql = boundSql.getSql();
        Select select = (Select) CCJSqlParserUtil.parse(sql);
        SelectBody selectBody = select.getSelectBody();

        if(selectBody == null || !(selectBody instanceof PlainSelect) || CollectionUtils.isNotEmpty(select.getWithItemsList())) {
            // 普通查询
            throw new MpsException("Only support common query sql , execute sql:" + sql);
        }
        SqlCache sqlCache = buildSqlCache(methodId, (PlainSelect) selectBody);

        return sqlCache;
    }

    private static SqlCache buildSqlCache(String methodId, PlainSelect selectBody) {
        Map<String,String> tableNameMap = new HashMap<>();
        PlainSelect plainSelect = selectBody;
        FromItem fromItem = plainSelect.getFromItem();

        SqlCache sqlCache = new SqlCache();
        sqlCache.setTableNameMap(tableNameMap);
        setTableAliasNameMap(sqlCache,fromItem);
        sqlCache.setMethodId(methodId);
        List<Join> joins = plainSelect.getJoins();
        if(joins != null) {
            for (int i = 0; i < joins.size(); i++) {
                Join join = joins.get(i);
                FromItem rightItem = join.getRightItem();
                setTableAliasNameMap(sqlCache, rightItem);
            }
        }

        Expression where = selectBody.getWhere();

        Expression append = new StringValue(DEFAULT_WHERE_REPLACE_KEY);
        // sql的where条件设置占位符， 动态生成的where过滤条件  替换这个占位符
        sqlCache.setWhereReplaceKey("'" + DEFAULT_WHERE_REPLACE_KEY + "'");
        if(where == null){
            sqlCache.setContainsWhere(false);
            selectBody.setWhere(append);
            String sql = selectBody.toString();
            // 没有where条件 第一个连接符不起任何作用 因此两种sql都一样
            sqlCache.setAndSql(sql);
            sqlCache.setOrSql(sql);
        }else{
            sqlCache.setContainsWhere(true);
            if (where instanceof OrExpression) {
                OrExpression orExpression = new OrExpression(new Parenthesis(where), append);
                selectBody.setWhere(orExpression);
                sqlCache.setOrSql(selectBody.toString());

                AndExpression andExpression = new AndExpression(new Parenthesis(where), append);
                selectBody.setWhere(andExpression);
                sqlCache.setAndSql(selectBody.toString());
            } else {
                selectBody.setWhere(new OrExpression(where, append));
                sqlCache.setOrSql(selectBody.toString());

                selectBody.setWhere(new AndExpression(where, append));
                sqlCache.setAndSql(selectBody.toString());
            }
        }

        resolveMapperMethod(sqlCache,methodId);
        return sqlCache;
    }

    private static void resolveMapperMethod(SqlCache sqlCache,String methodId){
        int index = methodId.lastIndexOf(".");
        if(index == -1){
            return;
        }
        String className = methodId.substring(0,index);
        String methodName = methodId.substring(index + 1);
        Class clazz = null;
        try {
            clazz = Class.forName(className);
        }catch (Exception e){
            log.warn("Mapper class [" + className + "]  not found.");
            return;
        }
        Method [] methods = clazz.getMethods();
        Method targetMethod = null;
        for (int i = 0; i < methods.length; i++) {
            if(methods[i].getName().equals(methodName)){
                targetMethod =  methods[i];
            }
        }
        if(targetMethod == null){
            return;
        }
        sqlCache.setMethod(targetMethod);
        Class returnType = targetMethod.getReturnType();
        sqlCache.setPagination(IPage.class.isAssignableFrom(returnType));
    }
    
    private static void setTableAliasNameMap(SqlCache sqlCache,FromItem fromItem){
        if(fromItem instanceof Table){
            Table table = (Table)fromItem;
            Alias alias = table.getAlias();
            if(alias != null){
                String tableName = table.getName().toLowerCase();
                Set<String> repeatTable = sqlCache.getRepeatTableSet();
                if(repeatTable.contains(tableName)){
                    return;
                }

                Map<String,String> tableNameMap = sqlCache.getTableNameMap();
                if(tableNameMap.containsKey(tableName)){
                    tableNameMap.remove(tableName);
                    repeatTable.add(tableName);
                }else{
                    tableNameMap.put(tableName,alias.getName());
                }
            }
        }
    }

    public static class SqlCache{
        /**
         *  表别名映射
         *  key   表名
         *  value 表别名
         */
        private Map<String,String> tableNameMap = new HashMap<>();

        /**
         * 一条sql语句中出现过两次的表名集合
         */
        private Set<String> repeatTableSet = new HashSet<>();

        /**
         * mapper 函数 ID
         */
        private String methodId;

        /**
         * 函数
         */
        private Method method;

        /**
         * 是否是分页查询
         * 根据method 返回类型判断是不是 IPage子实现类
         */
        private boolean pagination;

        /**
         *  sql语句
         *  SELECT * FROM table t1 where t1.name = 1 and #{whereReplaceKey}
         */
        private String andSql;

        /**
         *  sql语句
         *  SELECT * FROM table t1 where t1.name = 1 or #{whereReplaceKey}
         */
        private String orSql;

        /**
         *  替换的关键字
         */
        private String whereReplaceKey;

        /**
         * where关键字 是否存在
         */
        private boolean containsWhere;


        public Map<String, String> getTableNameMap() {
            return tableNameMap;
        }

        public void setTableNameMap(Map<String, String> tableNameMap) {
            this.tableNameMap = tableNameMap;
        }

        public Set<String> getRepeatTableSet() {
            return repeatTableSet;
        }

        public void setRepeatTableSet(Set<String> repeatTableSet) {
            this.repeatTableSet = repeatTableSet;
        }

        public String getMethodId() {
            return methodId;
        }

        public void setMethodId(String methodId) {
            this.methodId = methodId;
        }

        public Method getMethod() {
            return method;
        }

        public void setMethod(Method method) {
            this.method = method;
        }

        public boolean isPagination() {
            return pagination;
        }

        public void setPagination(boolean pagination) {
            this.pagination = pagination;
        }

        public String getAndSql() {
            return andSql;
        }

        public void setAndSql(String andSql) {
            this.andSql = andSql;
        }

        public String getOrSql() {
            return orSql;
        }

        public void setOrSql(String orSql) {
            this.orSql = orSql;
        }

        public String getWhereReplaceKey() {
            return whereReplaceKey;
        }

        public void setWhereReplaceKey(String whereReplaceKey) {
            this.whereReplaceKey = whereReplaceKey;
        }

        public boolean isContainsWhere() {
            return containsWhere;
        }

        public void setContainsWhere(boolean containsWhere) {
            this.containsWhere = containsWhere;
        }
    }

}
