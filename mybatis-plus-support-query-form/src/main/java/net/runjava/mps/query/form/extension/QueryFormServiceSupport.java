package net.runjava.mps.query.form.extension;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import net.runjava.mps.BaseMapperSupport;
import net.runjava.mps.query.form.QueryForm;

import java.util.List;
import java.util.Map;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 16:41
 */
public interface QueryFormServiceSupport<T>{

    BaseMapper<T> getBaseMapper();

    default Long selectCount(QueryForm<T> queryForm) {
        return this.getBaseMapper().selectCount(queryForm.buildQuery());
    }

    default List<T> selectList(QueryForm<T> queryForm) {
        return this.getBaseMapper().selectList(queryForm.buildQuery());
    }

    default List<T> selectList(QueryForm<T> queryForm, String... selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        queryWrapper.select(selectColumns);
        return this.getBaseMapper().selectList(queryWrapper);
    }

    default List<T> selectList(QueryForm<T> queryForm, List selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        queryWrapper.select(selectColumns);
        return this.getBaseMapper().selectList(queryWrapper);
    }

    default List<T> selectList(QueryForm<T> queryForm, SFunction<T,Object>... selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        List<String> columns  = ((BaseMapperSupport)this.getBaseMapper()).getColumns(selectColumns);
        queryWrapper.select(columns);
        return this.getBaseMapper().selectList(queryWrapper);
    }

    default <P extends IPage<T>> P selectPage(QueryForm<T> queryForm) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        return (P) this.getBaseMapper().selectPage(queryForm.buildPage(),queryWrapper);
    }

   
    default <P extends IPage<T>> P selectPage(QueryForm<T> queryForm, SFunction<T, Object>... selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        List<String> columns  = ((BaseMapperSupport)this.getBaseMapper()).getColumns(selectColumns);
        queryWrapper.select(columns);
        return (P) this.getBaseMapper().selectPage(queryForm.buildPage(),queryWrapper);
    }

   
    default <P extends IPage<T>> P selectPage(QueryForm<T> queryForm, String... selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        queryWrapper.select(selectColumns);
        return (P) this.getBaseMapper().selectPage(queryForm.buildPage(),queryWrapper);
    }

   
    default <P extends IPage<T>> P selectPage(QueryForm<T> queryForm, List selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        queryWrapper.select(selectColumns);
        return (P) this.getBaseMapper().selectPage(queryForm.buildPage(),queryWrapper);
    }


    default <P extends IPage<Map<String, Object>>> P selectMapsPage(QueryForm<T> queryForm) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        return (P) this.getBaseMapper().selectMapsPage(queryForm.buildPage(),queryWrapper);
    }

    default <P extends IPage<Map<String, Object>>> P selectMapsPage(QueryForm<T> queryForm, SFunction<T, Object>... selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        List<String> columns  = ((BaseMapperSupport)this.getBaseMapper()).getColumns(selectColumns);
        queryWrapper.select(columns);
        return (P) this.getBaseMapper().selectMaps(queryForm.buildPage(),queryWrapper);
    }

    default <P extends IPage<Map<String, Object>>> P selectMapsPage(QueryForm<T> queryForm, String... selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        queryWrapper.select(selectColumns);
        return (P) this.getBaseMapper().selectMaps(queryForm.buildPage(),queryWrapper);
    }

    default <P extends IPage<Map<String, Object>>> P selectMapsPage(QueryForm<T> queryForm, List selectColumns) {
        QueryWrapper queryWrapper = queryForm.buildQuery();
        queryWrapper.select(selectColumns);
        return (P) this.getBaseMapper().selectMaps(queryForm.buildPage(),queryWrapper);
    }

}
