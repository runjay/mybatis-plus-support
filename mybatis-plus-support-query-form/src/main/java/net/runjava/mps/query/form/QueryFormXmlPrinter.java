package net.runjava.mps.query.form;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import net.runjava.mps.exception.MpsException;
import net.runjava.mps.query.form.annotation.Where;
import net.runjava.mps.query.form.metadata.WhereField;
import net.runjava.mps.query.form.metadata.WhereGroupData;
import net.runjava.mps.utils.ArrayUtils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * @author Running
 * @since 2024/3/18 18:24
 */
public class QueryFormXmlPrinter {

    private List<WhereGroupData> groups;

    private QueryForm queryForm;


    public QueryFormXmlPrinter(List<WhereGroupData> groups,QueryForm queryForm){
        this.groups = groups;
        this.queryForm = queryForm;
    }

    /**
     * 若开发者不想使用自动注入sql方式，可以手动调用该函数生成并输出mybatis where条件到控制台并复制到对应的XML文件中去
     */
    public void printWhereXml(String formParamName){

        StringBuffer xmlBuffer = new StringBuffer();

        xmlBuffer.append("<where>\r\n");

        for (int i = 0; i < groups.size(); i++) {
            WhereGroupData groupData = groups.get(i);

            boolean outside = groupData.isOutside();
            if(!outside){
                xmlBuffer.append("\t " + groupData.getConcatType().name() + "( 1 = 1 \r\n");
            }

            boolean first = true;
            List<WhereField> whereFields = groupData.getItems();
            for (int j = 0; j < whereFields.size(); j++) {
                WhereField whereField = whereFields.get(j);
                Where where = whereField.getWhere();

                Field field = whereField.getField();
                String tableAliasName = where.tableAliasName();
                if(StringUtils.isEmpty(tableAliasName)){
                    tableAliasName = whereField.getTableName();
                }

                String columnName = tableAliasName + "." + whereField.getColumnName();
                String columnValueKey = formParamName + "." + field.getName();

                xmlBuffer.append("\t" + getIfSql(whereField,columnName,columnValueKey) + "\r\n");
                if(first){
                    first = false;
                    xmlBuffer.append(" \t\tAND ");
                }else{
                    xmlBuffer.append(" \t\t" + whereField.getWhere().concatType().name() + " ");
                }
                xmlBuffer.append(getFieldConditionSql(whereField,columnName,columnValueKey) + "\r\n");
                xmlBuffer.append("\t</if>\r\n");
            }

            if(!groupData.isOutside()){
                xmlBuffer.append("\t) \r\n");
            }
        }
        xmlBuffer.append("</where>");
        System.err.println("Generate queryForm:[" + queryForm.getClass().getName() + "] Mybatis sql : ");
        System.err.println(xmlBuffer);
    }


    private String getIfSql(WhereField whereField,String columnName,String columnValueKey){
        Field field = whereField.getField();
        Class fieldType = field.getType();
        Where.Type type = whereField.getWhere().type();
        if(type == Where.Type.IN || type == Where.Type.NOT_IN || type == Where.Type.BETWEEN){
            int minLength = type == Where.Type.BETWEEN ? 1 : 0;
            boolean isArray = ArrayUtils.isArray(fieldType);
            if(isArray){
                return "<if " + columnValueKey + " != null and " + columnValueKey + ".length > " + minLength + " >";
            }else{
                return "<if " + columnValueKey + " != null and " + columnValueKey + ".size() > " + minLength + " >";
            }
        }

        if(fieldType == String.class){
            return "<if " + columnValueKey + " != null and " + columnValueKey + "!='' >";
        }

        return "<if " + columnValueKey + " != null >";
    }

    private String getFieldConditionSql(WhereField whereField,String columnName,String columnValueKey){
        Where where = whereField.getWhere();
        Where.Type type = where.type();

        switch (type){
            case EQ:
                return  columnName + " = #{" + columnValueKey + "}";
            case BETWEEN:
                return  columnName + " BETWEEN #{" + columnValueKey + "[0]} AND #{" + columnValueKey + "[1]}";
            case GT:
                return  columnName + " &gt; #{" + columnValueKey + "}";
            case GE:
                return  columnName + " &gt;= #{" + columnValueKey + "}";
            case LT:
                return  columnName + " &lt; #{" + columnValueKey + "}";
            case LE:
                return  columnName + " &lt;= #{" + columnValueKey + "}";
            case LIKE:
                return  columnName + " like concat('%',#{" + columnValueKey + "},'%'"+")";
            case LIKE_RIGHT:
                return  columnName + " like concat(#{" + columnValueKey + "},'%'"+")";
            case LIKE_LEFT:
                return  columnName + " like concat('%',#{" + columnValueKey + "})";
            case IS_NULL:
                return  columnName + " IS NULL";
            case NOT_NULL:
                return  columnName + " IS NOT NULL";
            case NOT_IN:
                return  columnName + " NOT IN \r\n"
                  + "\t\t<foreach collection=\"" + columnValueKey + "\" item=\"val\" open=\"(\" separator=\",\" close=\")\">\r\n"
                  +  "\t\t\t#{val}\r\n"
               +"\t\t</foreach>";
            case IN:
                return  columnName + " IN \r\n"
                        + "\t\t<foreach collection=\"" + columnValueKey + "\" item=\"val\" open=\"(\" separator=\",\" close=\")\">\r\n"
                        +  "\t\t\t#{val}\r\n"
                        +"\t\t</foreach>";
        }
        throw new MpsException("Undefined type [" + type.name() + "]");
    }
}
