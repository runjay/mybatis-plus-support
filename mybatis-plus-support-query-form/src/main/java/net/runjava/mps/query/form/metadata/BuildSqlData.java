package net.runjava.mps.query.form.metadata;

import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;

import java.util.List;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/8 19:47
 */

public class BuildSqlData {

    private String queryFormKey;

    private PluginUtils.MPBoundSql mpBoundSql;

    private MappedStatement mappedStatement;

    private List<ParameterMapping> parameterMappings;


    public String getQueryFormKey() {
        return queryFormKey;
    }

    public void setQueryFormKey(String queryFormKey) {
        this.queryFormKey = queryFormKey;
    }

    public PluginUtils.MPBoundSql getMpBoundSql() {
        return mpBoundSql;
    }

    public void setMpBoundSql(PluginUtils.MPBoundSql mpBoundSql) {
        this.mpBoundSql = mpBoundSql;
    }

    public MappedStatement getMappedStatement() {
        return mappedStatement;
    }

    public void setMappedStatement(MappedStatement mappedStatement) {
        this.mappedStatement = mappedStatement;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void setParameterMappings(List<ParameterMapping> parameterMappings) {
        this.parameterMappings = parameterMappings;
    }
}
