package net.runjava.mps.query.form.metadata;

import net.runjava.mps.query.form.annotation.ConcatType;

import java.util.List;

/**
 * @author Running
 * @since 2024/3/18 18:06
 */
public class WhereGroupColumnValueData {

    public WhereGroupColumnValueData(WhereGroupData data,List<ColumnValuePair> valuePairs){
        this.outside = data.isOutside();
        this.groupName = data.getGroupName();
        this.concatType = data.getConcatType();
        this.valuePairs = valuePairs;
    }

    /**
     * 最外层，不增加括号
     */
    private boolean outside;

    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 分组连接方式
     */
    private ConcatType concatType;


    private List<ColumnValuePair> valuePairs;


    public boolean isOutside() {
        return outside;
    }

    public String getGroupName() {
        return groupName;
    }

    public ConcatType getConcatType() {
        return concatType;
    }

    public List<ColumnValuePair> getValuePairs() {
        return valuePairs;
    }

}
