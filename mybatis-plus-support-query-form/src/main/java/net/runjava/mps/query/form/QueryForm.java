package net.runjava.mps.query.form;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import net.runjava.mps.query.form.metadata.WhereGroupColumnValueData;

import java.util.List;


/**
 * @author Running
 * @version 1.0
 * @since 2024/3/6 19:55
 */
public interface QueryForm<T>{

    /**
     * @return 构建分页参数
     */
    IPage buildPage();


    /**
     * @return 构建主表查询条件
     */
    QueryWrapper buildQuery();


    /**
     * @return 生成原生查询非空字段的数据集
     */
    List<WhereGroupColumnValueData> buildNativeQueryFormData();


    /**
     * @return 是否自动在原生sql的where条件中追加过滤条件
     */
    default boolean autoAppendNativeSql(){
        return true;
    }

    void printMybatisWhereXml();

    /**
     * 若开发者不想使用自动注入sql方式，可以手动调用该函数生成并输出mybatis where条件到控制台并复制到对应的XML文件中去
     * @param queryFormParamName  Mybatis参数中 queryForm的参数名
     */
    void printMybatisWhereXml(String queryFormParamName);

}
