package net.runjava.mps.starter.spring.boot3;

import net.runjava.mps.injector.MapperSupportSqlInjector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Running
 * @version 1.0
 */

@Configuration
public class MpsBasicSupportAutoConfig {

    @Bean
    public MapperSupportSqlInjector mapperSupportSqlInjector(){
        /**
         * mybatis-plus增强操作
         */
        return new MapperSupportSqlInjector();
    }

}
