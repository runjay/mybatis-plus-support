# 版本升级
version=1.0.0

echo "开始替换pom.xml的版本号：${version}"
mvn versions:set -DnewVersion=${version}

#echo "开始commit到本地仓库：${version}"
git commit -am "版本升级：${version}"

#echo "开始打tag：v${version}"
git tag -a v${version} -m "版本号：${version}"

#echo "开始提交到远程git仓库：${version}"
git push origin main --tags

#echo "开始发布新的版本到maven仓库：${version}"
mvn clean deploy -Dmaven.test.skip=true -pl mybatis-plus-support-annotation,mybatis-plus-support-core,mybatis-plus-support-query-form,mybatis-plus-support-starter -am