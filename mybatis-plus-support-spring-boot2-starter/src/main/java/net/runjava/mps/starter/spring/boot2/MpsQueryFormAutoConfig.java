package net.runjava.mps.starter.spring.boot2;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import net.runjava.mps.query.form.interceptor.QueryFormInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 17:19
 */
@Configuration
public class MpsQueryFormAutoConfig {

    @Bean
    public MybatisPlusInterceptor baseMybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // queryForm 拦截器必须在前，要不然会导致参数顺序错乱
        interceptor.addInnerInterceptor(new QueryFormInterceptor());
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }

}
