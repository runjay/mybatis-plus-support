package net.runjava.mps;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.support.LambdaMeta;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.core.toolkit.support.SerializedLambda;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.reflection.property.PropertyNamer;
import net.runjava.mps.exception.MpsException;
import net.runjava.mps.utils.ColumnCacheUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

/**
 * @author Running
 * @since 2024/2/19 17:04
 */
public interface BaseMapperSupport<T> extends BaseMapper<T> {


    /**
     *  获取一行记录并加锁 ，只在事务内生效
     *  select * from table where id = #{id} for update
     * @param id
     * @return 锁定的记录
     */
    @Deprecated
    T lock(Serializable id);

    /**
     * 不推荐直接使用这个方式
     * 该方式只是为了给 incrUpdateById
     * @param sql
     * @param param
     * @return 影响行数
     */
    @Deprecated
    int _updateBySql(@Param("sql") String sql,@Param("et") Object param);



    /**
     * 用于判断表是否存在相同值的记录
     * 如果存在主键,先过滤掉本身
     *  例如： exists(test,Test::name,Test:age)
     *      判断数据库是否存在一条 name跟age值都跟test一致的数据
     * @param model
     * @param columns
     * @return 记录是否存在
     */
    default boolean exists(T model, SFunction<T, Object>  ... columns){
        if(columns.length == 0){
            throw new RuntimeException("columns is empty");
        }
        
        Class<?> cls = model.getClass();
        TableInfo tableInfo = TableInfoHelper.getTableInfo(cls);
        String keyProperty = tableInfo.getKeyProperty();
        String keyColumn = tableInfo.getKeyColumn();
        Object idVal = ReflectionKit.getFieldValue(model, keyProperty);

        QueryWrapper<T> queryWrapper = new QueryWrapper<T>();
        if(StringUtils.checkValNotNull(idVal)){
            queryWrapper.ne(keyColumn,idVal);
        }
        Map<String,String> fieldMap = ColumnCacheUtils.getColumnMap(cls);
        for (int i = 0; i < columns.length; i++) {
            SFunction<T, Object> function = columns[i];
            SerializedLambda s = SerializedLambda.extract(function);
            String fieldName = PropertyNamer.methodToProperty(s.getImplMethodName());
            Object fieldValue = ReflectionKit.getFieldValue(model, fieldName);
            queryWrapper.eq(fieldMap.get(fieldName),fieldValue);
        }
        queryWrapper.last(" limit 1 ");
        return this.selectCount(queryWrapper) > 0;
    }

    /**
     *  根据ID自增且更新一条记录
     * incrCols的非空字段会自增对应的值     update table set a = a + value
     * 不在incrCols内的字段的非空值，会执行覆盖更新   update table set a = value
     *   例如 model
     *
     *
         TenConfig config = new TenConfig();
         config.setDataType(1); //自增1
         config.setParentId(2L); //自增2
         config.setUpdateTime(new Date()); //更新成当前时间
         config.setName("aaaa");    //更新名称
         baseMapper.incrUpdateById(config,TenConfig::getDataType,TenConfig::getParentId);
     * @param model
     *              需要更新对象
     * @param incrCols
     *             列出需要自增的字段
     * @return 是否更新成功
     */
    default boolean incrUpdateById(@Param("et") T model, SFunction<T, Object>  ... incrCols){
        if(incrCols.length == 0){
            throw new RuntimeException("incrCols is empty");
        }

        Class<?> cls = model.getClass();
        TableInfo tableInfo = TableInfoHelper.getTableInfo(cls);
        String keyProperty = tableInfo.getKeyProperty();
        if(StringUtils.isEmpty(keyProperty)){
            throw new RuntimeException(model.toString() +" do not have primary key");
        }
        Object idVal = ReflectionKit.getFieldValue(model, keyProperty);
        if(idVal == null){
            throw new RuntimeException("update model's id is empty");
        }
        Set<String> incrColSet = new HashSet<>();
        for (int i = 0; i < incrCols.length; i++) {
            SFunction<T, Object> function = incrCols[i];
            SerializedLambda s = SerializedLambda.extract(function);
            String fieldName = PropertyNamer.methodToProperty(s.getImplMethodName());
            if(fieldName.equals(keyProperty)){
                throw new MpsException("Key column not support increment");
            }
            incrColSet.add(fieldName);
        }
        StringBuffer sqlBuffer = new StringBuffer("update " + tableInfo.getTableName() + " set ");
        boolean first = true;

        List<TableFieldInfo> fieldList = tableInfo.getFieldList();
        for(TableFieldInfo fieldInfo : fieldList){
            Field field = fieldInfo.getField();
            String colName = fieldInfo.getColumn();
            String propertyName = field.getName();
            boolean incr = incrColSet.contains(propertyName);
            Object fieldValue = ReflectionKit.getFieldValue(model, propertyName);
            if(fieldValue == null){
                continue;
            }
            if(first){
                first = false;
            }else{
                sqlBuffer.append(",");
            }
            if(incr){
                if((fieldValue instanceof Number) == false){
                    throw new RuntimeException("Column [" + propertyName + "] use increment, but it is not a number type ");
                }
                sqlBuffer.append(colName + "=" + colName + " + #{et."+ propertyName + "}"  );
            }else{
                sqlBuffer.append(colName + "=#{et."+ propertyName + "}");
            }
        }
        sqlBuffer.append(" where " + tableInfo.getKeyColumn() + "= #{et."+keyProperty+ "} ");
        return _updateBySql(sqlBuffer.toString(),model) > 0;
    }

    default Class getMapperClass(){
        try{
            return (Class) getClass().getGenericInterfaces()[0];
        }catch (Exception e){
            throw new MpsException("can't get mapper class ",e);
        }
    }

    default Class getModelClass(){
        Class mapperClass = getMapperClass();
        try {
            ParameterizedType parameterizedType = ((ParameterizedType) mapperClass.getGenericInterfaces()[0]);
            return (Class)parameterizedType.getActualTypeArguments()[0];
        }catch (Exception e){
            throw new MpsException("can't get model class ",e);
        }
    }

    default List<String> getColumns(SFunction<T,Object> ... columns){
        Class modelClass = this.getModelClass();
        Map<String,String> columnMap = ColumnCacheUtils.getColumnMap(modelClass);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < columns.length; i++) {
            LambdaMeta lambdaMeta = LambdaUtils.extract(columns[i]);
            String fieldName = PropertyNamer.methodToProperty(lambdaMeta.getImplMethodName());
            String column = columnMap.get(fieldName);
            if(column != null){
                list.add(column);
            }
        }
        return list;
    }

    /**
     * @return 获取排除之后的字段集合
     */
    default List<String> getColumnsAfterExcluded(SFunction<T,Object> ... excludeColumns){
        Set<String> excludeColumnSet = new HashSet<>();
        for (int i = 0; i < excludeColumns.length; i++) {
            SFunction<T, Object> function = excludeColumns[i];
            SerializedLambda s = SerializedLambda.extract(function);
            excludeColumnSet.add(LambdaUtils.formatKey(PropertyNamer.methodToProperty(s.getImplMethodName())));
        }

        Class modelClass = this.getModelClass();

        Map<String,String> columnMap = ColumnCacheUtils.getColumnMap(modelClass);

        List<String> list = new ArrayList<>();
        for (String key : columnMap.keySet()) {
            if(excludeColumnSet.contains(key)){
                continue;
            }
            String column = columnMap.get(key);
            if(column != null){
                list.add(column);
            }
        }

        if(CollectionUtils.isEmpty(list)){
            /**
             *  当查询返回集合为空时，queryWrapper.select(selectColumns); 默认是查询所有字段
             *  当排除所有字段时，会返回的是空集时. 因此此处强制不允许返回空集合 (排除所有字段该查询也毫无意义)
             */
            throw new MpsException("select columns is empty");
        }
        return list;
    }
}
