package net.runjava.mps.injector.methods;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import net.runjava.mps.constants.MpsConstants;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.scripting.defaults.RawSqlSource;

/**
 * @author Running
 */
public class Lock extends AbstractMethod {

    public Lock() {
        super(MpsConstants.METHOD_NAME_LOCK);
    }

    /**
     * 通过id锁住一行记录
     */
    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {

        tableInfo.havePK();
        String primaryKey = tableInfo.getKeyColumn();
        String sql = "SELECT " + tableInfo.getAllSqlSelect() + " FROM " + tableInfo.getTableName() + " WHERE " + primaryKey + " = #{"+primaryKey+"} FOR UPDATE ";
        SqlSource sqlSource = new RawSqlSource(configuration, sql, modelClass);
        return super.addSelectMappedStatementForTable(mapperClass, MpsConstants.METHOD_NAME_LOCK, sqlSource,tableInfo);
    }
}
