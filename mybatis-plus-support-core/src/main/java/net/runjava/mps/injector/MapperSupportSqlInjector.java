package net.runjava.mps.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import net.runjava.mps.injector.methods.Lock;
import net.runjava.mps.injector.methods.UpdateBySql;

import java.util.List;

/**
 *  在mybatis-plus基础上加入增强函数
 * @author Running
 * @version 1.0
 */
public class MapperSupportSqlInjector extends DefaultSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
        List<AbstractMethod> methodList = super.getMethodList(mapperClass,tableInfo);
        if(tableInfo.havePK()){
            methodList.add(new Lock());
        }
        methodList.add(new UpdateBySql());
        return methodList;
    }

}
