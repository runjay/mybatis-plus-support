package net.runjava.mps.injector.methods;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import net.runjava.mps.constants.MpsConstants;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * 仅框架内部时使用，不建议直接调用此函数进行更新操作
 * @author Running
 */
public class UpdateBySql extends AbstractMethod {

    public UpdateBySql() {
        super(MpsConstants.METHOD_NAME_UPDATE_BY_SQL);
    }

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        String sql = "${sql}";
        SqlSource sqlSource = this.languageDriver.createSqlSource(this.configuration, sql, modelClass);
        return this.addUpdateMappedStatement(mapperClass, modelClass, MpsConstants.METHOD_NAME_UPDATE_BY_SQL,sqlSource);
    }

}
