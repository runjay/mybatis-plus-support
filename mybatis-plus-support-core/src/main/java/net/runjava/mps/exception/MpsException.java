package net.runjava.mps.exception;

/**
 * @author Running
 * @version 1.0
 */
public class MpsException extends RuntimeException{

    public MpsException(String msg){
        super(msg);
    }

    public MpsException(String msg,Throwable e){
        super(msg,e);
    }

}
