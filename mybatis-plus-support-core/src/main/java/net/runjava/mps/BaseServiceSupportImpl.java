package net.runjava.mps;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @author Running
 * @version 1.0
 */
public class BaseServiceSupportImpl<M extends BaseMapperSupport<T>, T> extends ServiceImpl<M, T> implements BaseServiceSupport<T> {

    @Override
    public List<String> getColumnsAfterExcluded(SFunction<T, Object>... excludeColumns) {
        return baseMapper.getColumnsAfterExcluded(excludeColumns);
    }

}
