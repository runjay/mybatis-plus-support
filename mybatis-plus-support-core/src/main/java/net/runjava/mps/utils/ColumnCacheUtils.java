package net.runjava.mps.utils;


import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import net.runjava.mps.exception.MpsException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Running
 * @version 1.0
 */
public class ColumnCacheUtils {

    /**
     *  key : model类
     *  value: Map<String,String>
     *          子Map的key : 属性名
     *          子map的value : 数据库字段名
     */
    private static Map<Class, Map<String, String>> COLUMN_CACHE_MAP = new ConcurrentHashMap<>();


    public static Map<String,String> getColumnMap(Class model){
        Map<String,String> fieldMap = COLUMN_CACHE_MAP.get(model);
        if(fieldMap == null){
            fieldMap =  initCache(model);
        }
        return fieldMap;
    }

    public static String getColumn(Class model,String property){
        return getColumnMap(model).get(property);
    }


    private static Map<String,String> initCache(Class model){
        synchronized (model){
            TableInfo tableInfo = TableInfoHelper.getTableInfo(model);
            if(tableInfo == null){
                throw new MpsException("class [" + model.getName() + "] is not a model");
            }
            HashMap fieldMap = new HashMap();
            List<TableFieldInfo> fieldList = tableInfo.getFieldList();
            int size = fieldList.size();
            for (int i = 0; i < size; i++) {
                TableFieldInfo fieldInfo = fieldList.get(i);
                fieldMap.put(fieldInfo.getField().getName(),fieldInfo.getColumn());
            }

            String keyProperty = tableInfo.getKeyProperty();
            if(StringUtils.isNotBlank(tableInfo.getKeyProperty())){
                fieldMap.put(keyProperty,tableInfo.getKeyColumn());
            }
            COLUMN_CACHE_MAP.put(model,fieldMap);
            return fieldMap;
        }
    }

}
