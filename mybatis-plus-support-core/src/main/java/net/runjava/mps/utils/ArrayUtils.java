package net.runjava.mps.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Running
 * @version 1.0
 */
public class ArrayUtils {
    /**
     * 判断是否原生数组
     */
    public static boolean isArray(Object object){
        return object.getClass().isArray();
    }

    public static boolean isArray(Class type){
        return type.isArray();
    }


    public static List toList(Object array){
        if(array instanceof Object[]){
            return Arrays.asList((Object[])array);
        }

        List list = new ArrayList();
        int length = Array.getLength(array);
        for (int i = 0; i < length; i++) {
            list.add(Array.get(array,i));
        }
        return list;
    }


}
