package net.runjava.mps.constants;

/**
 * @author Running
 * @since 2024/3/14 17:36
 */
public interface MpsConstants {


    /**
     * updateBySql的函数名
     */
    String METHOD_NAME_UPDATE_BY_SQL = "_updateBySql";


    /**
     * lock的函数名
     */
    String METHOD_NAME_LOCK = "lock";




}
