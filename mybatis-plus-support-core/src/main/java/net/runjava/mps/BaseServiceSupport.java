package net.runjava.mps;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Running
 * @version 1.0
 * @since 2024/3/13 16:36
 */
public interface BaseServiceSupport<T> extends IService<T> {

    /**
     * @param excludeColumns
     * @return 获取排除之后的字段集合
     */
    List<String> getColumnsAfterExcluded(SFunction<T,Object>... excludeColumns);



}
